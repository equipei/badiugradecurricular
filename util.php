<?php

class enrol_badiugcurricular_util  {


     public function enrol_status_options(){
 		$op=array();
        $op[1]=get_string('desabled','enrol_badiugcurricular');
        $op[0]=get_string('enabled','enrol_badiugcurricular');
        return $op;
 	}
 
 
	public function option_yes_not(){
 		$op[0]=get_string('no','enrol_badiugcurricular');
        $op[1]=get_string('yes','enrol_badiugcurricular');
        return $op;
 	}
  
  public function option_gradeperc(){
    $op=array();
     $op['']="--";
     for($i=0;$i<=100;$i++) {
           $op[$i]="$i %";
     }
 		
   return $op;
 	}


    public function option_role(){
        $op=array();
        $op[100]= get_string('typerole_onecourseconclusionbygrade', 'enrol_badiugcurricular');
        $op[200]= get_string('typerole_onecourseconclusionbycompletation', 'enrol_badiugcurricular');
        return $op;
    }
}
