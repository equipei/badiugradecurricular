<?php

require_once('../../../config.php'); 
require_once("$CFG->dirroot/enrol/badiugcurricular/replicategrouplib.php");


require_login();
require_capability('moodle/site:config', context_system::instance());

$param= new  stdClass;
$param->courseid = optional_param('courseid', 0, PARAM_INT);
$param->exec = optional_param('exec', 0, PARAM_INT);
gexec($param);
function gexec($param){
  $listcourses=courses_enable_badiugcurricular($param->courseid);
    if(empty($listcourses)){
        echo "Nenhuma regra de inscricao badiugcurricular foi localizado no sistema ";
        return null;
    }
   foreach ($listcourses as $crow) {
        $listusers=get_users_without_group($crow->id,$crow->coursetargetid);
        echo "<h3> ".$crow->coursetargetid." - ". $crow->fullname." | ". $crow->id." - ". $crow->fullname." </h3><br />";
        $total=count($listusers);
        echo "Total de inscricao sem grupo: $total <br />";
        $cont=0;

        if(!empty($listusers)){
            if($param->exec){
                foreach ($listusers as $lrow) {
                     replicategrouplib($crow->coursesourceid,$crow->coursetargetid,$lrow->userid);
                     echo "Id do usuario processado: ".$lrow->userid."<br />";
                     $cont++;
                }
                echo "Total de inscricao adcionado no grupo conforme a configuração do curso pre-requisito:  $cont <br />"; 
             }else{
                echo "Processamento de inscricao no grupo nao efeturado <br />"; 
             }
            
        }
        echo "<hr>";
    }
}


function replicategrouplib ($sourcecourseid,$targetcourseid,$userid){
    $lib=new enrol_badiugcurricular_replicategrouplib($sourcecourseid,$targetcourseid,$userid);
    $lib->exec();
}

function courses_enable_badiugcurricular($courseid=null){
    $wsql="";
    if(!empty($courseid)){ $wsql=" AND e.courseid=$courseid ";}
    global $CFG,$DB;   
	$sql="SELECT e.id,e.name,e.courseid AS coursetargetid,c.fullname,e.customint2 AS coursesourceid FROM {$CFG->prefix}enrol e INNER JOIN {$CFG->prefix}course c ON c.id=e.courseid WHERE e.enrol='badiugcurricular' AND e.customchar3='1' $wsql ";
	$r=$DB->get_records_sql($sql);
    return $r;
    
}

function get_users_without_group($enrolid,$courseid){
    global $CFG,$DB;   
	$sql="SELECT ue.userid FROM {$CFG->prefix}user_enrolments ue WHERE ue.enrolid = $enrolid AND (SELECT COUNT(m.id) FROM {$CFG->prefix}groups_members m INNER JOIN {$CFG->prefix}groups g ON g.id=m.groupid WHERE g.courseid=$courseid AND m.userid=ue.userid) = 0";
	$r=$DB->get_records_sql($sql);
    return $r;
    
}
?>
