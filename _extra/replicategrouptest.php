<?php

require_once('../../../config.php'); 
require_once("$CFG->dirroot/enrol/badiugcurricular/replicategrouplib.php");

require_login();
require_capability('moodle/site:config', context_system::instance());

//replicategrouplib (43,44,5); 

function replicategrouplib ($sourcecourseid,$targetcourseid,$userid){
    $lib=new enrol_badiugcurricular_replicategrouplib($sourcecourseid,$targetcourseid,$userid);
    $lib->exec();
}
?>
