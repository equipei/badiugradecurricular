<?php
require_once("$CFG->dirroot/enrol/badiugcurricular/gradelib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/enrollib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/grouplib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/replicategrouplib.php");
$ebgctask=new enrol_badiugcurricular_tasklib();
$ebgctask->exec();
class enrol_badiugcurricular_tasklib  {

	function exec(){
	
		//get list of enrol role
		$roles=$this->get_enable_cron_role();
		foreach ($roles as $role) {
			$currentcourse=$role->customint2;
			$configappovegrade=$role->customint5;
			$nextcourse=$role->courseid;
			$usersapp=null;
			
			//type role grade
			if($role->customint4==100 || empty($role->customint4)){
				$appovegrade=$this->get_approved_grade($currentcourse,$configappovegrade);
				$usersapp=$this->get_enabled_students_grade($appovegrade,$currentcourse,$nextcourse);
			}
			 else  if($role->customint4==200){
				$usersapp=$this->get_enabled_students_progress($currentcourse,$nextcourse);
			 }
			 foreach ($usersapp as $userap) {
				$userid=$userap->userid;
				//echo "user: ". $userid;
				$this->process_enrol($role,$userid);
				
				if($role->customint8){
					$userinfo=$this->get_user($userid);
					$messgeconf=$role->customtext3;
					$subjectconf=$role->customchar3;
					$currentcourseinfo=$this->get_course($currentcourse);
					$nextcourseinfo=$this->get_course($nextcourse); 
					$message=$this->message_fill_tag($messgeconf,$userinfo,$currentcourseinfo,$nextcourseinfo);
					$subject=$this->message_fill_tag($subjectconf,$userinfo,$currentcourseinfo,$nextcourseinfo);
					
					$this->send_message($role,$userinfo,$subject,$message);
				}
				
			 }
			
			
		
		}
	}
     function get_enabled_students_grade($appovegrade,$currentcourse,$nextcourse){
			global $CFG,$DB;
			$sql="SELECT g.userid FROM {$CFG->prefix}grade_items i INNER JOIN {$CFG->prefix}grade_grades g ON i.id=g.itemid  WHERE i.itemtype = 'course' AND i.courseid=$currentcourse AND g.finalgrade >= $appovegrade AND  g.userid NOT IN (SELECT DISTINCT rs.userid  FROM mdl_role_assignments rs INNER JOIN mdl_context e ON rs.contextid=e.id WHERE e.contextlevel=50 AND e.instanceid=$nextcourse ) ";
			 $rows=$DB->get_records_sql($sql);
			return $rows;
		}
	
	
	function get_enabled_students_progress($currentcourse,$nextcourse){
			global $CFG,$DB;
			$sql="SELECT userid  FROM {$CFG->prefix}course_completions  WHERE course=$currentcourse AND timecompleted > 0 AND userid NOT IN (SELECT DISTINCT rs.userid  FROM mdl_role_assignments rs INNER JOIN mdl_context e ON rs.contextid=e.id WHERE e.contextlevel=50 AND e.instanceid=$nextcourse ) ";
			 $rows=$DB->get_records_sql($sql);
			return $rows;
		}
		
 function get_enable_cron_role(){
     global $CFG,$DB;
	$sql="SELECT e.id,e.name,e.status,e.courseid,e.customint1,e.customint2,e.customint3,e.customint4,e.customint5,e.customint6,e.customint7,e.customint8,e.roleid,e.enrolperiod,e.customtext1,e.customtext2,e.customtext3,e.customchar1,e.customchar2,e.customchar3   FROM {$CFG->prefix}enrol e INNER JOIN {$CFG->prefix}course c ON c.id=e.courseid WHERE c.visible=1  AND e.customint7=1 ";
	$rows=$DB->get_records_sql($sql);
	return $rows;
 }
 
 function process_enrol($role,$userid){
		$enrollib=new enrol_badiugcurricular_enrollib();
		$enrollib->add_enrol($role->id,$userid,$role->courseid,$role->roleid,$role->enrolperiod);
		$group=new enrol_badiugcurricular_grouplib($role->courseid); 
		$group->add_member($role->customint6,$userid);
		if($role->customchar3){
			$replicategrouplib=new enrol_badiugcurricular_replicategrouplib($role->customint2,$role->courseid,$userid);
			$replicategrouplib->exec();
		  }
		echo "$userid enrol in course ".$role->courseid." ";
		
 }
 
 function send_message($role,$userinfo,$subject,$message){
		
		$siteinfo=$this->get_course(1);
        $userinfo->mailformat=1;
		$userid=$userinfo->id;
		$sendemail=email_to_user($userinfo, $siteinfo->shortname, $subject,'', $message);
        if($sendemail){echo "mail send to user $userid";}
		else {echo "mail not send to user $userid";}
 }
 
 function get_user($id){
		global $DB, $CFG;
		$sql ="SELECT id,firstname,lastname,email FROM {$CFG->prefix}user WHERE id= $id";
		return $DB->get_record_sql($sql);
    }

	function get_course($id){
				global $DB, $CFG;
			global $USER;
			$sql ="SELECT id,fullname,shortname	 FROM {$CFG->prefix}course WHERE id=$id";
			 return $DB->get_record_sql($sql);
			}
	function message_fill_tag($messge,$userinfo,$currentcourseinfo,$nextcourseinfo) {
			global $CFG;
			
			$currentcourseurl=$CFG->httpswwwroot."/course/view.php?id=".$currentcourseinfo->id;
			$currentcourselink="<a href=\"".$currentcourseurl."\"  target=\"_blank\">$currentcourseurl</a>";
			
			$nextcourseurl=$CFG->httpswwwroot."/course/view.php?id=".$nextcourseinfo->id;
			$nextcourselink="<a href=\"".$nextcourseurl."\"  target=\"_blank\">$nextcourseurl</a>";
			
			$fullname=$userinfo->firstname;
			if(!empty($userinfo->lastname)){$fullname=$userinfo->firstname." ".$userinfo->lastname;}
			
			 
			$messge=str_replace("{USER_FIRSTNAME}", $userinfo->firstname, $messge);
			$messge=str_replace("{USER_FULLNAME}", $fullname, $messge);
			
			$messge=str_replace("{COURSE_APPROVED_NAME}", $currentcourseinfo->fullname, $messge);
			$messge=str_replace("{COURSE_APPROVED_SHORTNAME}", $currentcourseinfo->shortname, $messge);
			$messge=str_replace("{COURSE_APPROVED_URL}", $currentcourseurl, $messge);
			$messge=str_replace("{COURSE_APPROVED_LINK}", $nextcourselink, $messge);
			
			$messge=str_replace("{COURSE_ENROLED_NAME}", $nextcourseinfo->fullname, $messge);
			$messge=str_replace("{COURSE_ENROLED_SHORTNAME}", $nextcourseinfo->shortname, $messge);
			$messge=str_replace("{COURSE_ENROLED_URL}", $nextcourseurl, $messge);
			$messge=str_replace("{COURSE_ENROLED_LINK}", $nextcourselink, $messge);
			
		return $messge;
	}
 function get_approved_grade($courseid,$gradapproverelative){
      $grade=new enrol_badiugcurricular_grade($courseid);
	  $itemid=$grade->get_itemid();
	  $scale=$grade->get_escale($itemid);
	  $approvedgrade=null;
	 
	  if( $scale >= 0 && $gradapproverelative>=0){
		$approvedgrade=$gradapproverelative*$scale/100;
	  }
	  return $approvedgrade;
 }
 
}
