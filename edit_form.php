<?php


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once("$CFG->dirroot/enrol/badiugcurricular/enrollib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/util.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/grouplib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/drule/baserole.php");



class enrol_badiugcurricular_edit_form extends moodleform {

   function definition() {

        global $DB,$dto,$courseid,$group;
        $manege=new enrol_badiugcurricular_enrollib();
        $ebutil=new enrol_badiugcurricular_util();  
		$group=new enrol_badiugcurricular_grouplib($courseid); 
		$drole= new badiu_badiugcurricular_baserole();
		
		$course_options=$manege->get_courses($courseid);
		
        $role_options = get_default_enrol_roles(context_system::instance());
       $typerole=$ebutil->option_role();
        $mform = $this->_form;
      
        
        list($instance, $plugin, $context) = $this->_customdata;

        $mform->addElement('header', 'header', get_string('configgeral', 'enrol_badiugcurricular'));

        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $dto->id);
       
        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT);
         $mform->setDefault('courseid', $dto->courseid);
        
        $mform->addElement('text', 'name', get_string('instancename', 'enrol_badiugcurricular'));
        $mform->setType('name', PARAM_TEXT);
         $mform->setDefault('name', $dto->name);
         $mform->addRule('name', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');
      
        $mform->addElement('select', 'status', get_string('status', 'enrol_badiugcurricular'), $ebutil->enrol_status_options());
       // $mform->addHelpButton('status', 'status', 'enrol_badiugcurricular');
         $mform->setDefault('status', $dto->status);
          $mform->addRule('status', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');
      
         $mform->addElement('select', 'customint1', get_string('allownewenrol', 'enrol_badiugcurricular'), $ebutil->option_yes_not());
        $mform->setType('customint1', PARAM_INT);
          $mform->setDefault('customint1', $dto->customint1);
         $mform->addRule('customint1', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');

       $mform->addElement('select', 'customint4', get_string('typerole', 'enrol_badiugcurricular'), $typerole);
       $mform->setType('customint4', PARAM_INT);
       $mform->setDefault('customint4', $dto->customint4);
       $mform->addRule('customint4', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');


       $mform->addElement('select', 'customint2', get_string('courseconditional', 'enrol_badiugcurricular'), $course_options);
        $mform->setType('customint2', PARAM_INT);
          $mform->setDefault('customint2', $dto->customint2);
        $mform->addRule('customint2', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');
         
        $mform->addElement('select', 'customint5', get_string('gradecondition', 'enrol_badiugcurricular'),$ebutil->option_gradeperc());
        $mform->setType('customint5', PARAM_INT);
         $mform->setDefault('customint5', $dto->customint5);
       // $mform->addRule('customint5', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');
       $mform->disabledIf('customint5', 'customint4', 'eq', 200);

        $mform->addElement('select', 'roleid', get_string('defaultrole', 'enrol_badiugcurricular'), $role_options);
        $mform->setType('roleid', PARAM_INT);
       $mform->setDefault('roleid', $dto->roleid);
        $mform->addRule('roleid', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');
        
		
		$mform->addElement('select', 'customchar3', get_string('replicategroupenrol', 'enrol_badiugcurricular'), $ebutil->option_yes_not());
        $mform->setType('customchar3', PARAM_INT);
          $mform->setDefault('customchar3', $dto->customchar3);
         $mform->addRule('customchar3', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');

		
		$mform->addElement('select', 'customint6', get_string('group', 'enrol_badiugcurricular'), $group->get_groups());
        $mform->setType('customint6', PARAM_INT);
        $mform->setDefault('customint6', $dto->customint6);
	$mform->disabledIf('customint6', 'customchar3', 'eq', 1);
        
        $mform->addElement('text', 'customint3', get_string('maxuserenrol', 'enrol_badiugcurricular'));
        $mform->setType('customint3', PARAM_INT);
         $mform->setDefault('customint3', $dto->customint3);
        
        $mform->addElement('duration','enrolperiod',  get_string   ('enrolvalidate','enrol_badiugcurricular'), array('optional' => true, 'defaultunit' => 86400));
        $mform->setType('enrolperiod', PARAM_INT);
         $mform->setDefault('enrolperiod', $dto->enrolperiod);
         
        $mform->addElement('editor', 'customtext1', get_string('msgpresentationapproval','enrol_badiugcurricular'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('customtext1', PARAM_RAW);
         $mform->setDefault('customtext1', $dto->customtext1);
         $mform->addRule('customtext1', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');
       
        $mform->addElement('editor', 'customtext2', get_string('msgpresentationdisapproval','enrol_badiugcurricular'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('customtext2', PARAM_RAW);
         $mform->setDefault('customtext2', $dto->customtext2);
         $mform->addRule('customtext2', get_string('requiredfield','enrol_badiugcurricular'), 'required', null, 'cliente');
       
       	$mform->addElement('header', 'processerolcron', get_string('processerolcron', 'enrol_badiugcurricular'));
		
       
		$mform->addElement('select', 'customint7', get_string('processerolcron', 'enrol_badiugcurricular'), $ebutil->option_yes_not());
        $mform->setType('customint7', PARAM_INT);
          $mform->setDefault('customint7', $dto->customint7);
        
		$mform->addElement('select', 'customint8', get_string('processerolcronsendmail', 'enrol_badiugcurricular'), $ebutil->option_yes_not());
        $mform->setType('customint8', PARAM_INT);
          $mform->setDefault('customint8', $dto->customint8);
      	$mform->disabledIf('customint8', 'customint7', 'eq', 0);
		
		$mform->addElement('text', 'customchar3', get_string('processerolcronmailsubject', 'enrol_badiugcurricular'),'size="60"');
        $mform->setType('customchar3', PARAM_TEXT);
        $mform->setDefault('customchar3', $dto->customchar3);
		$mform->disabledIf('customchar3', 'customint8', 'eq', 0);
		
		 $mform->addElement('editor', 'customtext3', get_string('processerolcronmailmessage','enrol_badiugcurricular'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('customtext3', PARAM_RAW);
         $mform->setDefault('customtext3', $dto->customtext3);
        $mform->disabledIf('customtext3', 'customint8', 'eq', 0);
	
	   
     /*  dinamyc role desabled
		$mform->addElement('select', 'customchar1', get_string('dreuleexecbefore', 'enrol_badiugcurricular'),$drole->get_options());
        $mform->setType('customchar1', PARAM_TEXT);
        $mform->setDefault('customchar1', $dto->customchar1);
        
		$mform->addElement('select', 'customchar2', get_string('dreuleexecafter', 'enrol_badiugcurricular'), $drole->get_options('after'));
        $mform->setType('customchar2', PARAM_TEXT);
        $mform->setDefault('customchar2', $dto->customchar2);
		*/
		 $this->add_action_buttons(true, ($instance->id ? null : get_string('addinstance', 'enrol')));
        $this->set_data($instance);
    }

    function validation($data, $files) {
        global $DB, $CFG;
        $errors = parent::validation($data, $files);

        if(empty($data['customint2'])){
            $errors['customint2'] = get_string('requiredfield','enrol_badiugcurricular');
        }

        return $errors;
    }

}
