<?php

 class mdl_plugin_config  {
 	
      /**
     * @var string
     */
    private $name;
 	
    function __construct($name) {
       $this->name=$name;
    }
  
    function get_all_as_array() {
         global $CFG,$DB;
       $sql="SELECT name,value FROM {$CFG->prefix}config_plugins WHERE  plugin ='".$this->getName()."'";
        $rows=$DB->get_records_sql($sql);
        $list=array();
       if(!empty($rows)){
            foreach ($rows as $row) {
                  $list[$row->name]=$row->value;
            }
      }
      return $list;
    }   
	
	
function cast_list_to_dto($list){
	$dto=new  stdClass;
       $dto->status=$list['status'];
       $dto->customint1=$list['allownewenrol'];
       $dto->customint2=null;
       $dto->customint3=null;
       $dto->customint4=100;//onecourseconclusionbygrade
       $dto->customdec1=null;
       $dto->customtext1=get_string('msgpresentationapproval_default','enrol_badiugcurricular');
       $dto->customtext2=get_string('msgpresentationdisapproval_default','enrol_badiugcurricular');
       $dto->enrolperiod=0;
       $dto->roleid=$list['roleid'];

		//cron
		$dto->customint7=1; //enable process cron
		$dto->customint8=1; //enable send mail
		$dto->customchar3=get_string('processerolcronmailsubject_default','enrol_badiugcurricular');
		$dto->customtext3=get_string('processerolcronmailmessage_default','enrol_badiugcurricular');
        return $dto;
	
	}
	
   function getDefaultValue(){
  
        $list= $this->get_all_as_array();
       
		if (array_key_exists("status", $list) && array_key_exists("roleid", $list)  ) {
    
				return $this->cast_list_to_dto($list);
		}    
       $dto=new  stdClass;
       $dto->status=0;
       $dto->customint1=1;
       $dto->customint2=null;
       $dto->customint3=null;
       $dto->customint4=100;
       $dto->customdec1=null;
       $dto->customtext1=null;
       $dto->customtext2=null;
       $dto->enrolperiod=0;
       $dto->roleid=5;
            
        return $dto;
       
        return $dto;
   }
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }


 }