<?php

class enrol_badiugcurricular_grouplib  {


      /**
     * @var integer
     */
    private $courseid;
 	
    function __construct($courseid) {
       $this->courseid=$courseid;
    }
	
	public function exist_group_in_course() {
        global $CFG,$DB;   
		$sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}groups WHERE courseid=".$this->courseid; 
		$r=$DB->get_record_sql($sql);
		return $r->countrecord;
    }

	public function exist_member($groupid,$userid) {
        global $CFG,$DB;   
		$sql="SELECT COUNT(id) AS countrecord  FROM {$CFG->prefix}groups_members WHERE groupid=$groupid AND  userid=$userid"; 
		$r=$DB->get_record_sql($sql);
		return $r->countrecord;
    }
	public function add_member($groupid,$userid) {
	
		if(empty($groupid) || empty($userid)) return -1;
		if($this->exist_member($groupid,$userid)) return -2;
        global $DB;
        $dto=new  stdClass;
		$dto->groupid=$groupid;
		$dto->userid=$userid;
   
		$result=$DB->insert_record('groups_members', $dto);
		return $result;
    }
	
	function get_groups(){
			global $DB, $CFG;
            $sql ="SELECT id,name FROM {$CFG->prefix}groups WHERE courseid=".$this->courseid; 
			$courses=$DB->get_records_sql($sql);
            $options = array();
			$options['']="  ----  ";
			foreach ($courses as $course){ 
                $options[$course->id]=$course->name;
			}
			return 	$options ;
    }
	 public function getCourseid() {
        return $this->courseid;
    }

    public function setCourseid($courseid) {
        $this->courseid = $courseid;
    }
}
