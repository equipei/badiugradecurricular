<?php

require('../../config.php'); 
require_once("$CFG->dirroot/enrol/badiugcurricular/locallib.php");  
require_once("$CFG->dirroot/enrol/badiugcurricular/plugin_config.php"); 

require_once("$CFG->dirroot/enrol/badiugcurricular/edit_form.php");

$courseid   = required_param('courseid', PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);

$course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);
require_capability('enrol/badiugcurricular:config', $context);

$PAGE->set_url('/enrol/badiugcurricular/edit.php', array('courseid'=>$course->id, 'id'=>$instanceid));
$PAGE->set_pagelayout('admin');

$return = new moodle_url('/enrol/instances.php', array('id'=>$course->id));
if (!enrol_is_enabled('badiugcurricular')) {
    redirect($return);
}

$dto= new  stdClass;
$lib=new enrol_badiugcurricular_lib();
$plugin=new mdl_plugin_config('enrol_badiugcurricular');     
          
if ($id) {
    $dto=$lib->get_by_id($id);
    $dto=$lib->cast_dto_editor_showformedit($dto);
} else {
    $dto->id       = null;
     $dto=$plugin->getDefaultValue();
     $dto=$lib->cast_dto_editor_showformedit($dto);
		 $dto->courseid = $course->id;
   
  }

  
  
  $form= new enrol_badiugcurricular_edit_form();  
 if ($form->is_cancelled()) {
       redirect($return);

  } else if ($formdata = $form->get_data()) {
           if($formdata->customchar3){$formdata->customint6=null;}
		   $formdata= $dto=$lib->cast_dto_editor_add($formdata);
		   $lib->save($formdata);
          redirect($return);
   }

$PAGE->set_heading($course->fullname);
$PAGE->set_title(get_string('pluginname', 'enrol_badiugcurricular'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'enrol_badiugcurricular'));
$form->display();
echo $OUTPUT->footer();
