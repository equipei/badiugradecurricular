<?php
require_once("$CFG->dirroot/group/lib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/grouplib.php");
class enrol_badiugcurricular_replicategrouplib {


      /**
     * @var integer
     */
    private $sourcecourseid;
    
    /**
     * @var integer
     */
    private $targetcourseid;
    
    /**
     * @var integer
     */
    private $userid;
 	
    function __construct($sourcecourseid,$targetcourseid,$userid) {
       $this->sourcecourseid=$sourcecourseid;
       $this->targetcourseid=$targetcourseid;
       $this->userid=$userid;
    }

    function exec() {
    
        //get group in source course
        $grouplibsource=new enrol_badiugcurricular_grouplib($this->sourcecourseid);
        //get target group / create group
        
        $existgroupsource=$this->exist_group_by_userid($this->sourcecourseid,$this->userid);

        if($existgroupsource){
            $listgroupsource=$this->get_group_by_userid($this->sourcecourseid,$this->userid);
            //add user in target group
            foreach ($listgroupsource as $row) {
                $row->courseid=$this->targetcourseid;
                $groupid=$this->get_groupid_by_idnumber($this->targetcourseid,$row->idnumber);
                if(empty($groupid)){$groupid=$this->get_groupid_by_name($this->targetcourseid,$row->name);}
                 
                //create group
                if(empty($groupid)){$groupid=$this->create_group($row);}

                //add user in group 
                if(!empty($groupid)){
                   $grouplib=new enrol_badiugcurricular_grouplib(null);
                   $grouplib->add_member($groupid,$this->userid);
                }
                 
           }
        }
        
    }
    /*
     * @param should with var:
     *      name - group name
     *      courseid - id of course
     *      idnumber - idnumber of group
     *      
     */
     public function create_group($param) {
         $result = groups_create_group($param);
         return $result;
    }
    
    public function exist_group_by_userid($courseid,$userid) {
        global $CFG,$DB;   
	$sql="SELECT COUNT(m.id) AS countrecord FROM {$CFG->prefix}groups g INNER JOIN {$CFG->prefix}groups_members m ON g.id=m.groupid WHERE g.courseid= $courseid AND m.userid = $userid "; 
	$r=$DB->get_record_sql($sql);
        if(empty($r)){return false;}
	return $r->countrecord;
    }
    
    public function get_group_by_userid($courseid,$userid) {
        global $CFG,$DB;   
	$sql="SELECT g.name,g.idnumber,courseid FROM {$CFG->prefix}groups g INNER JOIN {$CFG->prefix}groups_members m ON g.id=m.groupid WHERE g.courseid= $courseid AND m.userid = $userid "; 
	$r=$DB->get_records_sql($sql);
        if(empty($r)){return null;}
	return $r;
    }

    public function get_groupid_by_idnumber($courseid,$idnumber) {
        if(empty($idnumber)){return null;}
        global $CFG,$DB;   
	$sql="SELECT MIN(id) AS id FROM {$CFG->prefix}groups  WHERE courseid= $courseid AND idnumber= '".$idnumber."'"; 
	$r=$DB->get_record_sql($sql);
        if(empty($r)){return null;}
	return $r->id;
    }
    public function get_groupid_by_name($courseid,$name) {
        global $CFG,$DB;   
        if(empty($name)){return null;}
	$sql="SELECT MIN(id) AS id  FROM {$CFG->prefix}groups  WHERE courseid= $courseid AND name= '".$name."'"; 
	$r=$DB->get_record_sql($sql);
        if(empty($r)){return null;}
	return $r->id;
    }
    function getSourcecourseid() {
        return $this->sourcecourseid;
    }

    function getTargetcourseid() {
        return $this->targetcourseid;
    }

    function getUserid() {
        return $this->userid;
    }

    function setSourcecourseid($sourcecourseid) {
        $this->sourcecourseid = $sourcecourseid;
    }

    function setTargetcourseid($targetcourseid) {
        $this->targetcourseid = $targetcourseid;
    }

    function setUserid($userid) {
        $this->userid = $userid;
    }


}
