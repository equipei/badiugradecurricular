<?php

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->dirroot/enrol/badiugcurricular/util.php");
$ebutil=new enrol_badiugcurricular_util();  
$options=$ebutil->option_yes_not();
 $settings->add(new admin_setting_heading('enrol_badiugcurricular_settings', '', get_string('msgsglobalsetting', 'enrol_badiugcurricular')));
  
 if ($ADMIN->fulltree) {
     //field status
    $settings->add(new admin_setting_configselect('enrol_badiugcurricular/status',
		get_string('status', 'enrol_badiugcurricular'), get_string('status_desc', 'enrol_badiugcurricular'), 0, $ebutil->enrol_status_options()));

     // allow new enrol request
    $settings->add(new admin_setting_configselect('enrol_badiugcurricular/allownewenrol',
        get_string('allownewenrol', 'enrol_badiugcurricular'), get_string('allownewenrol_desc', 'enrol_badiugcurricular'), 1, $options));
        
        
  //defult role
     $options = get_default_enrol_roles(context_system::instance());
     $student = get_archetype_roles('student');
        $student = reset($student);
        $settings->add(new admin_setting_configselect('enrol_badiugcurricular/roleid',
            get_string('defaultrole', 'enrol_badiugcurricular'), get_string('defaultrole_desc', 'enrol_badiugcurricular'), $student->id, $options));
  
 }
 
  