<?php

class enrol_badiugcurricular_plugin extends enrol_plugin {

     public function get_info_icons(array $instances) {
      
    }
    
     public function roles_protected() {
   
        return false;
    }
    
     public function allow_unenrol(stdClass $instance) {
     
        return true;
    }
    
     public function allow_manage(stdClass $instance) {
      
        return true;
    }
    
    public function show_enrolme_link(stdClass $instance) {
        return ($instance->status == ENROL_INSTANCE_ENABLED);
    }
    
     public function add_course_navigation($instancesnode, stdClass $instance) {
        if ($instance->enrol !== 'badiugcurricular') {
             throw new coding_exception('Invalid enrol instance type!');
        }

        $context = context_course::instance($instance->courseid);
        if (has_capability('enrol/badiugcurricular:config', $context)) {
            $managelink = new moodle_url('/enrol/badiugcurricular/edit.php', array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $instancesnode->add($this->get_instance_name($instance), $managelink, navigation_node::TYPE_SETTING);
        }
    }
    public function get_newinstance_link($courseid) {
        $context = context_course::instance($courseid, MUST_EXIST);
        return new moodle_url('/enrol/badiugcurricular/edit.php', array('courseid'=>$courseid));
    }
      public function get_action_icons(stdClass $instance) {
        global $OUTPUT;

        if ($instance->enrol !== 'badiugcurricular') {
            throw new coding_exception('invalid enrol instance!');
        }
        $context = context_course::instance($instance->courseid);

        $icons = array();

             $editlink = new moodle_url("/enrol/badiugcurricular/edit.php", array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('t/edit', get_string('edit'), 'core',
                array('class' => 'iconsmall')));
       
        return $icons;
    }



       public function enrol_page_hook(stdClass $instance) {
     global $CFG, $OUTPUT,$USER;
        require_once("$CFG->dirroot/enrol/badiugcurricular/enrol.php");
       
       
         $enrol=new enrol_badiugcurricular_process($instance);
	 $enrolid= optional_param('enrolid', 0,PARAM_INT);
        $prcess=FALSE;
        if($enrolid > 0 && $enrolid==$instance->id){
           $prcess=TRUE;
        }
       $result= $enrol->exec($USER->id,$prcess);
         $result="<center>$result</center>";
       return $OUTPUT->box($result);
       
    }

     public function get_user_enrolment_actions(course_enrolment_manager $manager, $ue) {
         $actions = array();
        $context = $manager->get_context();
        $instance = $ue->enrolmentinstance;
        $params = $manager->get_moodlepage()->url->params();
        $params['ue'] = $ue->id;
        if ($this->allow_unenrol_user($instance, $ue) && has_capability("enrol/badiugcurricular:manage", $context)) {
            $url = new moodle_url('/enrol/unenroluser.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/delete', ''), get_string('unenrol', 'enrol'), $url, array('class'=>'unenrollink', 'rel'=>$ue->id));
        }
        if ($this->allow_manage($instance) && has_capability("enrol/badiugcurricular:manage", $context)) {
            $url = new moodle_url('/enrol/editenrolment.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/edit', ''), get_string('edit'), $url, array('class'=>'editenrollink', 'rel'=>$ue->id));
        }
        return $actions;
    }
    function cron(){
         echo "enrol_badiugcurricular precessando...";
           
    }
   
    
    public function can_delete_instance($instance) {
        $context = context_course::instance($instance->courseid);
        return has_capability('enrol/badiugcurricular:config', $context);
    }

    public function can_hide_show_instance($instance) {
        $context = context_course::instance($instance->courseid);
        return has_capability('enrol/badiugcurricular:config', $context);
    }
}
