Nome do plugin: badiugcurricular
Descrição: Gerenciar Inscrição com base no resultado do curso configurado como pré-requisito
Tipo de plugin: Plugin de matrícula
Versão do Moodle: 2.x,3.x
Distribuído por: Badiu – Soluções para Moodle
Site: http://www.badiu.net
Contato: linovazmoniz@gmail.com / lino@badiu.net
Skype: badiu.net


Funcionalidade: 
        - Configurar condição de inscrição com base na nota final e rastreamento de conclusão de um determinado curso. 
        Se o usuário acessar o curso, ele será inscrito caso atender o requisito de nota de aprovação ou  rastreamento de conclusão do curso pré-requisito
        configurado no plugin;
		
		-Na versão 1.6 lançado em 08/02/2019 foi adicionado opção de cadastrar usuário no mesmo grupo do curso pré-requisito
		-Na versão 1.5 lançado em 09/10/2018 foi feito correção da compatibilidade com PHP 7 para funcionar no Moodle  que requer PHP 7
		-Na versão 1.4 lançado em 15/09/2016 foi adicionado processamento de cron. Processar inscrição dos alunos que atendem pré-requisitos de grade curricular e enviar e-mail aos alunos informando sobre a inscrição no novo curso
		-Na versão 1.3 lançado em 28/04/2016 na 12º edição do Moodle Moot em São Paulo foi adicionado recurso grupo
Requisitos Técnicos:
        Deve ser instalado em qualquer versão do Moodle de família 2.x e 3.x



 