<?php
require_once("$CFG->dirroot/enrol/badiugcurricular/enrollib.php");

 class enrol_badiugcurricular_grade {
     /**
     * @var integer
     */
    private $courseid;
    
    /**
     * @var float
     */
    private $gradeincousecond;
    
     /**
     * @var boolean
     */
    private $error;
    
     /**
     * @var integer
     */
    private $coderror;
    
    function __construct($courseid) {
          $this->courseid=$courseid;
          $this->error=FALSE;
          $this->coderror=null;
          $this->gradeincousecond=null;
          
    }
        public function exist_itemid() {
              global $DB, $CFG;
             $sql="SELECT COUNT(id) AS qreg  FROM {$CFG->prefix}grade_items WHERE courseid=".$this->courseid. " AND itemtype='course'";
            
             $r=$DB->get_record_sql($sql);
	           return $r->qreg; 
            
     }
     
     public function get_itemid() {
              global $DB, $CFG;
             $sql="SELECT id  FROM {$CFG->prefix}grade_items WHERE courseid=".$this->courseid. " AND itemtype='course'";
             $r=$DB->get_record_sql($sql);
	           return $r->id; 
     }
     
     public function get_escale($itemid) {
              global $DB, $CFG;
              $sql="SELECT grademax FROM {$CFG->prefix}grade_items WHERE id=$itemid";
              $r=$DB->get_record_sql($sql);
	            return $r->grademax; 
     }
     
     public function exist_finalgrade($itemid,$userid) {
             global $DB, $CFG;
              $sql="SELECT COUNT(id) AS qreg  FROM {$CFG->prefix}grade_grades WHERE itemid=$itemid AND userid= $userid";
              $r=$DB->get_record_sql($sql);
	            return $r->qreg; 
     }
     
     public function get_finalgrade($itemid,$userid) {
             global $DB, $CFG;
              $sql="SELECT finalgrade FROM {$CFG->prefix}grade_grades WHERE itemid=$itemid AND userid= $userid";
              $r=$DB->get_record_sql($sql);
	            return $r->finalgrade; 
     }
     
     public function is_approval($userid,$percent_condition) {
         //existe itemid
           if($this->exist_itemid()==0){
                 $this->error=TRUE;
                 $this->coderror=101;
                 return null;
           }
           if($this->exist_itemid()>1){
                 $this->error=TRUE;
                 $this->coderror=102;
                 return null;
           }
         //get itemid
           $itemid=$this->get_itemid();
           if(empty($itemid)){
                 $this->error=TRUE;
                 $this->coderror=103;
                return null;
           }
           //get scale
           $scale=$this->get_escale($itemid);
           if(empty($scale)){
                 $this->error=TRUE;
                 $this->coderror=104;
                return null;
           }
          if(!$this->exist_finalgrade($itemid,$userid)){
              $this->gradeincousecond=null;
              return FALSE;
          }
         //get grade
          $grade=$this->get_finalgrade($itemid,$userid);
          $this->gradeincousecond=$grade;
          
           if(empty($percent_condition)){
                 $this->error=TRUE;
                 $this->coderror=105;
                return null;
           }
             
           $course_grade_perc=  $grade*100;
           $course_grade_perc=$course_grade_perc/$scale;
           
           if( $course_grade_perc<$percent_condition){
               return FALSE;
           }
           return TRUE;
     }
 }