<?php
require_once("$CFG->dirroot/enrol/badiugcurricular/enrollib.php");

 class enrol_badiugcurricular_progress {
     /**
     * @var integer
     */
    private $courseid;

     /**
      * @var integer
      */
     private $userid;

    
    function __construct($courseid, $userid) {
          $this->courseid=$courseid;
          $this->userid=$userid;

    }
        public function is_course_completed() {
              global $DB, $CFG;
             $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}course_completions WHERE userid=".$this->userid. " AND course=".$this->courseid. " AND timecompleted > 0 ";
             $r=$DB->get_record_sql($sql);
	         return $r->countrecord;
     }

 }