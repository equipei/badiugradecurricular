<?php
require_once("$CFG->dirroot/enrol/badiugcurricular/enrollib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/gradelib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/progresslib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/grouplib.php");
require_once("$CFG->dirroot/enrol/badiugcurricular/replicategrouplib.php");

 class enrol_badiugcurricular_process {

     /**
     * @var stdClass
     */
    private $instance;
    /**
     * @var stdClass
     */
    private $lib;
    function __construct(stdClass $instance) {
      $this->instance = $instance;
      $this->lib=new enrol_badiugcurricular_enrollib();
      }
   
      public function exec($userid,$process=FALSE) {
            //check if new enrol are allowed
             $result=$this->check_newenrol_allowed();
             if(!empty($result)) return $result;

            // check limit enrol user
            $result=$this->check_user_limit();
             if(!empty($result)) return $result;

			//exec rule before
			$this->exec_role_before($userid);
			
			
           // check grade condition e process enrol
          if($this->instance->customint4==100 || empty($this->instance->customint4)){
              $result= $this->check_grade_condition($userid,$process);
              return $result;
          }

          //check course comppletetion
          else  if($this->instance->customint4==200){
              $result= $this->check_course_completation($userid,$process);
              return $result;

          }

      }    
    
     public function check_newenrol_allowed(){
         if($this->instance->customint1 == 0){
                return get_string('msgnewenrolnotallowed', 'enrol_badiugcurricular');;
           }   
           return null;
    	 
    }
          
     public function check_user_limit(){
           if($this->instance->customint3==0) return null;
           $limituser=$this->lib->count_enrol($this->instance->id);
           if($limituser >= $this->instance->customint3){
                return get_string('msgmaxuserenrol', 'enrol_badiugcurricular');
           }   
           return null;
     }     
   
    public function check_grade_condition($userid,$process){
            global $DB;
            $condition= $this->has_grade_condition($userid);
            if($process && $condition){
                  $enrollib=new enrol_badiugcurricular_enrollib();
                  $enrollib->add_enrol($this->instance->id,$userid,$this->instance->courseid,$this->instance->roleid,$this->instance->enrolperiod);
                  $luser = $DB->get_record('user', array('id'=>$userid));
                  complete_user_login($luser);
				  $group=new enrol_badiugcurricular_grouplib($this->instance->courseid); 
                  $group->add_member($this->instance->customint6,$userid);
                  if($this->instance->customchar3){
                    $replicategrouplib=new enrol_badiugcurricular_replicategrouplib($this->instance->customint2,$this->instance->courseid,$userid);
                    $replicategrouplib->exec();
                  }
                return get_string('msgenrolsuccess', 'enrol_badiugcurricular');
            } else{
                 if($condition)  {
                         $result=$this->instance->customtext1;
                         $result.="<br />";
                         $url="/enrol/index.php?id=".$this->instance->courseid."&enrolid=".$this->instance->id;
                         $result.=$this->link_button(get_string('doenrol','enrol_badiugcurricular'),$url);
                         
                        return $result;
                 }
                  else {return $this->instance->customtext2;}
            }
            return null;
           }   
    
    
    public function has_grade_condition($userid){
      
          if($this->instance->customint5==0) return TRUE;
          $grade=new enrol_badiugcurricular_grade($this->instance->customint2);
          $result=$grade->is_approval($userid,$this->instance->customint5);
           return $result;
        
    }


     public function check_course_completation($userid,$process){
        global $DB;
         $couseid=$this->instance->customint2;
         $progresslib=new  enrol_badiugcurricular_progress($couseid,$userid);
         $coursecompleted=$progresslib->is_course_completed();
         if($process && $coursecompleted){
             $enrollib=new enrol_badiugcurricular_enrollib();
             $enrollib->add_enrol($this->instance->id,$userid,$this->instance->courseid,$this->instance->roleid,$this->instance->enrolperiod);
             $luser = $DB->get_record('user', array('id'=>$userid));
             complete_user_login($luser);
             $group=new enrol_badiugcurricular_grouplib($this->instance->courseid); 
             $group->add_member($this->instance->customint6,$userid);
             if($this->instance->customchar3){
                $replicategrouplib=new enrol_badiugcurricular_replicategrouplib($this->instance->customint2,$this->instance->courseid,$userid);
                $replicategrouplib->exec();
              }
			 return get_string('msgenrolsuccess', 'enrol_badiugcurricular');
         } else{

             if($coursecompleted)  {
                 $result=$this->instance->customtext1;
                 $result.="<br />";
                 $url="/enrol/index.php?id=".$this->instance->courseid."&enrolid=".$this->instance->id;
                 $result.=$this->link_button(get_string('doenrol','enrol_badiugcurricular'),$url);

                 return $result;
             }
             else {return $this->instance->customtext2;}

         }

         return null;
     }
     public  function link_button($name_link=null,$relative_url=null){
           global $CFG;
           global $COURSE;
           if(empty($name_link)){$name_link=get_string('doenrol','enrol_badiugcurricular');}
            if(empty($relative_url)){$relative_url="/course/view.php?id=".$COURSE->id;}
           
            $url="'".$CFG->wwwroot.$relative_url."'";
         
         $hml='<form>';
           $hml.=' <input TYPE="button" VALUE="'.$name_link.'"';
           $hml.='  onclick="window.location.href='.$url.'"> ';
           $hml.=' </form>';
         //  $hml=' <a href='.$url.'>'.$name_link.'</a>';
           return $hml;
           
       }
      

     public  function exec_role_before($userid){
			global $CFG;
			$filerole=$this->instance->customchar1;
			if(!empty($filerole)){
				$pathfilerole="$CFG->dirroot/enrol/badiugcurricular/drule/before/$filerole";
				if(file_exists($pathfilerole)){
					require_once($pathfilerole);
					$this->instance=$baserole->exec($this->instance,$userid);
				}
			}
	 }
	  
      public function getInstance() {
          return $this->instance;
      }

      public function setInstance(stdClass $instance) {
          $this->instance = $instance;
      }


 }