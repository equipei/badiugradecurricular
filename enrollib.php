<?php

class enrol_badiugcurricular_enrollib  {

     function get_courses($except){
		global $DB, $CFG;
                 $sql ="SELECT id,fullname FROM {$CFG->prefix}course WHERE id> 1 && id!=$except ORDER BY fullname";
		$courses=$DB->get_records_sql($sql);
                
                $options = array();
		$options['']="  ----  ";
		foreach ($courses as $course){ 
                    	$options[$course->id]=$course->fullname;
		}
		return 	$options ;
           }
 function get_contextid($courseid){
     global $CFG,$DB;
     $sql="SELECT id FROM {$CFG->prefix}context WHERE contextlevel=50 AND instanceid=$courseid";
     $r=$DB->get_record_sql($sql);
     return $r->id;
 }
 
 function exist_role_assignments($userid,$contextid,$roleid){
      global $CFG,$DB;   
     $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}role_assignments WHERE contextid=$contextid AND userid=$userid AND roleid=$roleid ";
      $r=$DB->get_record_sql($sql);
     return $r->countrecord;
 }
 function save_role_assignments($userid,$contextid,$roleid){
      global $DB;
     $dto=new stdClass;
     $dto->roleid=$roleid;
     $dto->contextid=$contextid;
     $dto->userid=$userid;
   
     $result=$DB->insert_record('role_assignments', $dto);
     return $result;
 }
 
  function exist_user_enrolments($userid,$enrolid,$status){
     global $CFG,$DB;
     $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}user_enrolments WHERE userid=$userid AND enrolid=$enrolid AND status=$status ";
     $r=$DB->get_record_sql($sql);
     return $r->countrecord;
 }
 function save_user_enrolments($userid,$enrolid,$timestart,$timeend){
    global $DB;
     $dto=new stdClass;
     $dto->status=0;
     $dto->enrolid=$enrolid;
     $dto->userid=$userid;
     $dto->timestart=$timestart; 
     $dto->timeend=$timeend; 
     $dto->timecreated=time();
     
     $result=$DB->insert_record('user_enrolments', $dto);
     return $result;
 }
 
  function get_timeend($periodtemend){
      $timeend=0;
       if($periodtemend>0){
           $timeend=time()+$periodtemend;
       }
      return $timeend;
  }
 function add_enrol($enrolid,$userid,$courseid,$roleid,$periodtemend){
     global $CFG,$DB;
     $status=0;
     $timestart=time();
     $timeend=$this->get_timeend($periodtemend);
     $result=0;
     $contextid=$this->get_contextid($courseid);
    
     if(!$this->exist_role_assignments($userid,$contextid,$roleid)){
          $this->save_role_assignments($userid,$contextid,$roleid);
      }
     
      if(!$this->exist_user_enrolments($userid,$enrolid,$status)){
         $result= $this->save_user_enrolments($userid,$enrolid,$timestart,$timeend);
      }
    return  $result;
 } 
 
 function count_enrol($enrolid){
            global $DB, $CFG;
            $sql="SELECT COUNT(id) AS qreg FROM {$CFG->prefix}user_enrolments WHERE enrolid=$enrolid AND status=0";
            $r=$DB->get_record_sql($sql);
	    return $r->qreg; 
        }
}
