<?php
 
namespace enrol_badiugcurricular\task;
class cron_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('task', 'enrol_badiugcurricular');
    }

    public function execute() {
        global $CFG;
        echo 'badiu grade curricular';
		require_once("$CFG->dirroot/enrol/badiugcurricular/tasklib.php"); 
		 
    }

}
