<?php

 class enrol_badiugcurricular_lib {

   function save($dto) {
     global $CFG,$DB;
	 if($dto->id >0){return $this->edit($dto);}
     $dto->enrol="badiugcurricular";
     $dto->timemodified=time();
     $r=$DB->insert_record('enrol', $dto);
     return $r;
 }

 function edit($dto) {
     global $CFG,$DB;
     $dto->enrol="badiugcurricular";
     $dto->timemodified=time();
     $r= $DB->update_record('enrol', $dto);
     return  $r;
 }
 
 public function exist_by_id($id) {
             global $DB, $CFG;
              $sql="SELECT COUNT(id) AS qreg  FROM {$CFG->prefix}enrol WHERE id= $id";
              $r=$DB->get_record_sql($sql);
	            return $r->qreg; 
     }
  function get_by_id($id) {
              global $DB, $CFG;
              $sql="SELECT id,name,status,courseid,customint1,customint2,customint3,customint4,customint5,customint6,customint7,customint8,roleid,enrolperiod,customtext1,customtext2,customtext3,customchar1,customchar2,customchar3   FROM {$CFG->prefix}enrol WHERE id= $id";
              $r=$DB->get_record_sql($sql);
			  return $r; 
      }
	  
  function cast_dto_editor_add($dto) {
	if(isset($dto->customtext1['text'])){$dto->customtext1=$dto->customtext1['text'];}
	if(isset($dto->customtext2['text'])){$dto->customtext2=$dto->customtext2['text'];}
	if(isset($dto->customtext3['text'])){$dto->customtext3=$dto->customtext3['text'];}
	
	return $dto;
 }
 
  function cast_dto_editor_showformedit($dto) {
	 if(!empty($dto->customtext1)){
		$msg=array ('text'=>$dto->customtext1,'format' => 1 );
		$dto->customtext1=$msg;
	}
	
	if(!empty($dto->customtext2)){
		$msg=array ('text'=>$dto->customtext2,'format' => 1 );
		$dto->customtext2=$msg;
	}
	
	if(!empty($dto->customtext3)){
		$msg=array ('text'=>$dto->customtext3,'format' => 1 );
		$dto->customtext3=$msg;
	}
	return $dto;
  }
 }