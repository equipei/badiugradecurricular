<?php
$string['pluginname'] = 'Badiu Grade Curricular';
$string['msgshowonrequest'] = 'Método de inscrição Badiu Grade curricular.<br/> Coloque aqui a mensagem sobre regra de inscrição e o botão avançar. Faça isso na função enrol_page_hook que fica no arquivo lib.php';
$string['msgsglobalsetting'] = 'Defina aqui as configurações globais do plugin. Essas configurações serão herdadas automaticamente no formulário de configuração de cada instância do método de inscrição. Os parâmetros podem ser customizados cada vez que que um novo método de inscrição for instanciado no curso.';

$string['status'] ='Status';
$string['configgeral'] ='Configuração Geral';
$string['status_desc'] ='Define se o método de inscrição está ativo. Se estiver inativo, as inscrições efetivadas ficarão bloqueadas e novas inscrições não serão permitidas.';
$string['yes']='Sim';
$string['no']='Não';
$string['enabled']='Ativo';
$string['desabled']='Inativo';
$string['requiredfield'] = 'Campo obrigatório'; 

$string['allownewenrol']='Permitir novas inscrições';
$string['allownewenrol_desc']='Novas inscrições do curso  podem ser suspensas ou habilitadas com essa configuração.';
$string['defaultrole']='Papel atribuído por padrão';
$string['defaultrole_desc']='Define a função padrão que o usuário será inscrito no curso.';
$string['enrolvalidate']='Validade de inscrição';
$string['maxuserenrol']='Limite  de vaga';


$string['instancename']='Nome do método';
$string['courseconditional']='Curso pré-requisito';
$string['gradecondition']='Aproveitamento mínimo';
$string['msgpresentationapproval']='Mensagem de apresentação se o usuário atende os requisitos';
$string['msgpresentationdisapproval']='Mensagem de apresentação se o usuário não atende os requisitos';

$string['msgpresentationapproval_default']='Você atende os requisitos para inscrever neste curso.';
$string['msgpresentationdisapproval_default']='Você não atende os requisitos para inscrever neste curso.';

$string['msgnewenrolnotallowed']='As inscrições já foram encerradas.';
$string['msgmaxuserenrol']='O limite de vaga esgotado.'; 

$string['msggeneralfailure']='Ocorreu uma falha no processamento de inscrição.'; 
$string['msgenrolsuccess']='A sua inscrição foi feita com sucesso.'; 
$string['doenrol']='Inscreva no curso';

$string['typerole']='Critério de matrícula';
$string['typerole_onecourseconclusionbygrade']='Conclusão de um curso pelo critério de nota';
$string['typerole_onecourseconclusionbycompletation']='Conclusão de um curso pelo critério de rastreamento de progresso';

$string['group']='Grupo';

$string['dreuleexecbefore']='Executar regra adicional antes de processar a matrícula';
$string['dreuleexecafter']='Executar regra adicional depois de processar a matrícula';
$string['task']='Processar inscrição dos alunos que atendem pré-requisitos de grade curricular e enviar e-mail aos alunos informando sobre a inscrição no novo curso';
$string['processerolcron']='Processar inscrição automática pelo cron';
$string['processerolcronenabel']='Efetuar inscrição automática pelo cron';
$string['processerolcronsendmail']='Enviar e-mail ao aluno';
$string['processerolcronmailsubject']='Assunto do e-mail que será enviada ao aluno';
$string['processerolcronmailmessage']='Mensagem do e-mail que será enviada ao aluno';

$string['processerolcronmailsubject_default']='Inscrição no curso {COURSE_ENROLED_NAME}';
$string['processerolcronmailmessage_default']='Caro(a) <b>{USER_FIRSTNAME}</b><br />,Você já concluiu o curso {COURSE_APPROVED_NAME} e foi inscrito automaticamente no próximo  curso <b>{COURSE_ENROLED_NAME}</b>. Para acessar, clique no link {COURSE_ENROLED_LINK}';

$string['replicategroupenrol']='Replicar inscrição no grupo que existe no curso pré-requisito';
?>